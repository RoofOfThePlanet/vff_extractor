#include <iostream>
#include <cstring>
#include <filesystem>
#include "VFFArchive.h"
#include "ZfDecompressor.h"

typedef struct
{
	char *input_file;
	char *data_file;
	long long offset; // header offset;
	int mode;
} VFF_EXTRACTOR_OPT;

int parse_command_line(VFF_EXTRACTOR_OPT *opt,int argc,char **argv)
{
	int parsed_count = 0;
	if (opt == nullptr)
	{
		return parsed_count;
	}

	opt->input_file = nullptr;
	opt->data_file = nullptr;
	opt->mode = 0;
	opt->offset = 0;

	for(int i = 1;i < argc;++i)
	{
		if (_stricmp(argv[i],"--input") == 0 || _stricmp(argv[i],"-i") == 0)
		{
			if ((i + 1) < argc)
			{
				opt->input_file = argv[i + 1];
				++parsed_count;
			}
			++i;
			++parsed_count;
		}
		else if (_strnicmp(argv[i],"--data=",strlen("--data=")) == 0)
		{
			opt->data_file = argv[i] + strlen("--data=");
			++parsed_count;
		}
		else if (_stricmp(argv[i],"--list") == 0 || _stricmp(argv[i],"-l") == 0)
		{
			opt->mode = 0;
			++parsed_count;
		}
		else if (_stricmp(argv[i],"--extract") == 0 || _stricmp(argv[i],"-e") == 0)
		{
			opt->mode = 1;
			++parsed_count;
		}
		else if (_strnicmp(argv[i],"--offset=",strlen("--offset=")) == 0)
		{
			opt->offset = strtoll(argv[i] + strlen("--offset="),nullptr,16);
			++parsed_count;
		}
	}

	return parsed_count;
}

void enumerate_all(VFFArchive& archive)
{
	unsigned int num_files = archive.get_number_of_files();
	for(unsigned int i = 0;i < num_files;++i)
	{
		VFF_STORED_FILE_INFO info;
		archive.retrieve_file_information(&info,i);
		std::cout << (i + 1) << " / " << num_files << '\t';
		std::cout << info.name << '\t';
		std::cout << info.size << "bytes";
		std::cout << " at offset: " << info.offset << std::endl;
		archive.release_file_information(&info);
	}
	return;
}

unsigned int unz(VFFArchive& arc,unsigned int idx,__int64 offset,unsigned int sz,ZfDecompressor& decompressor)
{
	const unsigned int chunk_size = 1024*1024*16;
	unsigned int written_bytes = 0;
	for(;sz;)
	{
		unsigned int read_size = ((chunk_size < sz) ? chunk_size : sz);
		try
		{
			unsigned char *p = new unsigned char [read_size];
			arc.read_file_data_into_buffer(p,read_size,idx,offset);
			written_bytes += decompressor.decompress(p,read_size);
			delete [] p;
		}
		catch(std::bad_alloc& e)
		{
		}
		sz -= read_size;
		offset += read_size;
	}
	return written_bytes;
}

unsigned int copy_stream(VFFArchive& arc,unsigned int idx,__int64 offset,unsigned int sz,std::ofstream& ofs)
{
	const unsigned int chunk_size = 1024*1024*16;
	unsigned int written_bytes = 0;
	for(;sz;)
	{
		unsigned int read_size = ((chunk_size < sz) ? chunk_size : sz);
		try
		{
			unsigned char *p = new unsigned char [read_size];
			arc.read_file_data_into_buffer(p,read_size,idx,offset);
			ofs.write(reinterpret_cast<char*>(p),read_size);
			written_bytes += read_size;
			delete [] p;
		}
		catch(std::bad_alloc& e)
		{
		}
		sz -= read_size;
		offset += read_size;
	}
	return written_bytes;
}

void extract_all(VFFArchive& archive,char *dest = nullptr)
{
	namespace fs = std::experimental::filesystem;
	fs::path base_dest_dir = (dest) ? dest : fs::current_path();

	unsigned int num_files = archive.get_number_of_files();
	for(unsigned int i = 0;i < num_files;++i)
	{
		VFF_STORED_FILE_INFO info = {0};
		archive.retrieve_file_information(&info,i);

		fs::path dest_path = base_dest_dir / fs::path(info.name);
		fs::create_directories(dest_path.parent_path());
		std::cout << info.name;
		if (info.flag == 0)
		{
			ZfDecompressor decompressor(dest_path.string().c_str());
			unz(archive,i,0,info.size,decompressor);
		}
		else
		{
			// data is not compressed
			std::ofstream ofs(dest_path.string().c_str(),std::ofstream::binary);
			copy_stream(archive,i,0,info.size,ofs);
			ofs.close();
		}
		archive.release_file_information(&info);
		std::cout << std::endl;
	}
}

int main(int argc,char **argv)
{
	if (argc > 1)
	{
		VFF_EXTRACTOR_OPT opt;
		parse_command_line(&opt,argc,argv);
		if (opt.input_file)
		{
			VFFArchive archive;
			archive.open(opt.input_file,opt.offset,opt.data_file);
			archive.update();
			if (archive.is_valid())
			{
				switch(opt.mode)
				{
				case 0:
					enumerate_all(archive);
					break;
				case 1:
					extract_all(archive);
					break;
				default:
					break;
				}
			}
			else
			{
				std::cout << "Sorry, this archive is not valid or corrupted" << std::endl;
			}
			archive.close();
		}
		else
		{
			std::cout << "no input file." << std::endl;
		}
	}
	return 0;
}
