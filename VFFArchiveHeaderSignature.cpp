#include "VFFArchiveHeaderSignature.h"
#include <string>

VFFArchiveHeaderSignature::VFFArchiveHeaderSignature()
{
	memset(this->hdr,0,8);
}

VFFArchiveHeaderSignature::~VFFArchiveHeaderSignature()
{
}

bool VFFArchiveHeaderSignature::read(std::ifstream& file)
{
	file.read(this->hdr,6);
	return file.good();
}

bool VFFArchiveHeaderSignature::is_valid(void)
{
	const char magic[] = "vff";
	if (std::string(magic).compare(0,3,this->hdr,3) == 0)
	{
		if (this->hdr[3] == 0 && this->hdr[4] == 0 && this->hdr[5] == 0)
		{
			return true;
		}
	}
	// invalid signature
	return false;
}


