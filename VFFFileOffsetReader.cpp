#include <cstdint>
#include "VFFFileOffsetReader.h"

VFFFileOffsetReader::VFFFileOffsetReader()
{
}

VFFFileOffsetReader::~VFFFileOffsetReader()
{
	this->release();
}

void VFFFileOffsetReader::release(void)
{
	delete [] this->file_offset_list;
}

unsigned int VFFFileOffsetReader::read_multiple_entries(std::ifstream& file,unsigned int entry_count)
{
	uint32_t crypt_key = 0;

	auto next_key = [&](uint32_t *key) -> uint32_t {
		*key += ((*key) << 2);
		return *key += 0x75d6ee39;
	};

	auto decrypt_buffer = [&](unsigned char *buf,int len,uint32_t *key) -> void {
		for(int i = 0;i < len;++i)
		{
			*(buf + i) ^= next_key(key);
		}
	};

	this->release();
	this->file_offset_list = new (std::nothrow) int64_t[entry_count + 1]();
	if (!this->file_offset_list)
	{
		return 0;
	}

	for(unsigned int i = 0;i < (entry_count + 1);++i)
	{
		int64_t derived_key = (int32_t)next_key(&crypt_key);
		int64_t temp = 0;
		file.read((char*)&temp,8);
		temp ^= derived_key;
		*(this->file_offset_list + i) = temp;
	}

	this->num_entries = entry_count + 1;

	return entry_count;
}

std::string VFFFileOffsetReader::get_value_as_string(unsigned int index)
{
	if (this->num_entries > index)
	{
		return std::to_string(static_cast<long long>(*(this->file_offset_list + index)));
	}
	return std::string();
}

