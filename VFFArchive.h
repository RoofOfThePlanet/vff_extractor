#ifndef VFF_ARCHIVE_H
#define VFF_ARCHIVE_H

#include <fstream>
#include <vector>
#include "VFFArchiveHeader.h"
#include "VFFStoredFileInfo.h"

class VFFArchive
{
public:
	VFFArchive();
	virtual ~VFFArchive();

	bool open(char *header_file_name,std::streampos offset = 0,char *data_file_name = nullptr);
	bool close(void);

	void update(void);
	bool is_valid(void) { return this->header.is_valid(); };
	unsigned int get_number_of_files(void) { return this->header.get_entry_count(); };

	bool retrieve_file_information(VFF_STORED_FILE_INFO *info,unsigned int index = 0);
	void release_file_information(VFF_STORED_FILE_INFO *info);

	// read data of (file_index)-th file in archive (from specified offset) into buffer, and return number of read bytes.
	unsigned int read_file_data_into_buffer(unsigned char *buffer,unsigned int read_size,unsigned int file_index = 0,__int64 read_offset = 0);
private:
	void release_all(void);
	int load_additional_data_files(char *basefile);
	// do not delete *located_file
	bool locate_data_file_by_offset(__int64 offset,unsigned int *data_file_idx,__int64* adjusted_offset);

	std::streampos hdr_ofs = 0;
	__int64 first_data_size = 0;
	__int64 total_data_size = 0;
	VFFArchiveHeader header;
	std::ifstream header_file; // *.ext or *.dat
	std::vector<std::ifstream*> data_files; // *.dat, *.001, *.002, ...
};

#endif

