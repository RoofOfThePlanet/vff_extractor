#ifndef VFF_STORED_FILE_INFO_H
#define VFF_STORED_FILE_INFO_H

typedef struct tagVFF_STORED_FILE_INFO
{
	char *name;
	long long offset; // offset from the begining of vff sginature.
	long long size;
	int flag;
} VFF_STORED_FILE_INFO;

#endif
