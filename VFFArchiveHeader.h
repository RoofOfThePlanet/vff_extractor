#ifndef VFF_ARCHIVE_HEADER_H
#define VFF_ARCHIVE_HEADER_H

#include <vector>
#include "VFFArchiveHeaderSignature.h"
#include "IVFFArchiveDataReader.h"

class VFFArchiveHeader
{
public:
	VFFArchiveHeader();
	virtual ~VFFArchiveHeader();

	int read_from_file(std::ifstream& file);
	bool is_valid(void);

	unsigned int get_entry_count(void) { return this->num_entries; };
	std::string get_value_as_string(unsigned int file_index,unsigned int data_key);

	void reset(void);
private:
	void delete_all(void);

	VFFArchiveHeaderSignature signature; // holding signature ('vff')
	std::vector<IVFFArchiveDataReader*> HeaderData;

	unsigned int num_entries = 0;
	__int64 data_size = 0;
};

#endif

