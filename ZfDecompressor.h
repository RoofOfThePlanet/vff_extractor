#ifndef ZFDECOMPRESSOR_H
#define ZFDECOMPRESSOR_H

#include <fstream>
#include <zlib.h>

class ZfDecompressor
{
public:
	ZfDecompressor() = delete;
	ZfDecompressor(const ZfDecompressor& src) = delete;
	ZfDecompressor(ZfDecompressor&& src) = delete;

	// constructor with output file name
	ZfDecompressor(const char *out_file_name);

	virtual ~ZfDecompressor();

	// read n bytes data from the buffer and writes out decompressed data to file.
	unsigned int decompress(unsigned char *data,unsigned int n);
	// return true if input stream is end and further decompression is impossible.
	bool is_stream_end(void) { return this->prev_status == Z_STREAM_END; };

	bool has_error_occured(void) { return this->prev_status < 0; };
private:
	ZfDecompressor& operator = (ZfDecompressor& rhs);
	z_stream zstrm;
	std::ofstream out_file;
	const unsigned int chunk_size;
	bool is_initialized;
	int prev_status;
};

#endif

