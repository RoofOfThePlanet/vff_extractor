#include <cstdint>
#include "VFFFileFlagReader.h"

VFFFileFlagReader::VFFFileFlagReader()
{
}

VFFFileFlagReader::~VFFFileFlagReader()
{
	this->release();
}

void VFFFileFlagReader::release(void)
{
	delete [] this->file_flag_list;
}

unsigned int VFFFileFlagReader::read_multiple_entries(std::ifstream& file,unsigned int entry_count)
{
	this->release();
	this->file_flag_list = new (std::nothrow) int[entry_count]();

	if (!this->file_flag_list)
	{
		return 0;
	}

	for(unsigned int i = 0;i < entry_count;++i)
	{
		file.read((char*)(this->file_flag_list + i),1);
	}

	return this->num_entries = entry_count;
}

std::string VFFFileFlagReader::get_value_as_string(unsigned int index)
{
	if (this->num_entries > index)
	{
		return std::to_string(this->file_flag_list[index]);
	}
	return std::string();
}

