#ifndef VFF_ARCHIVE_HEADER_SIGNATURE_H
#define VFF_ARCHIVE_HEADER_SIGNATURE_H

#include <fstream>

class VFFArchiveHeaderSignature
{
public:
	VFFArchiveHeaderSignature();
	virtual ~VFFArchiveHeaderSignature();

	bool read(std::ifstream& file);
	bool is_valid(void);
private:
	char hdr[8];
};

#endif

