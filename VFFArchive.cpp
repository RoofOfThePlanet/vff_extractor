#include <sstream>
#include <iomanip>
#include "VFFArchive.h"

VFFArchive::VFFArchive()
{
}

VFFArchive::~VFFArchive()
{
	this->release_all();
}

bool VFFArchive::open(char *header_file_name,std::streampos offset,char *data_file_name)
{
	if (!this->header_file.is_open())
	{
		this->header_file.open(header_file_name,std::ifstream::binary);

		if (data_file_name)
		{
			this->data_files.push_back(new std::ifstream(data_file_name,std::ifstream::binary));
		}
		else
		{
			this->data_files.push_back(&this->header_file);
		}

		this->load_additional_data_files(header_file_name);

		this->hdr_ofs = offset;

		// calculate first data size
		this->header_file.seekg(offset,this->header_file.beg);
		std::ifstream *file = this->data_files[0];
		std::streampos init_pos = file->tellg();
		file->seekg(0,file->end);
		this->total_data_size += this->first_data_size = (file->tellg() - init_pos);
		file->seekg(init_pos);
		return true;
	}
	return false;
}

bool VFFArchive::close(void)
{
	this->release_all();
	return true;
}

void VFFArchive::release_all(void)
{
	if (this->header_file.is_open())
	{
		this->header_file.close();
	}
	for(auto& dat_file : this->data_files)
	{
		if (dat_file != &this->header_file)
		{
			delete dat_file;
		}
	}
	this->data_files.clear();
	this->hdr_ofs = 0;
	this->first_data_size = 0;
	this->total_data_size = 0;
}

int VFFArchive::load_additional_data_files(char *basefile)
{
	int loaded_files = 0;
	std::string base_file_name(basefile,strrchr(basefile,'.') - basefile);
	for(int i = 1;i < 1000;++i)
	{
		std::stringstream ss;
		// ss will be 001, 002, 003, ... , 999
		ss << base_file_name << '.' << std::setfill('0') << std::setw(3) << i;

		// open additional data files (if any)
		std::ifstream *ifs = new std::ifstream(ss.str(),std::ifstream::binary);
		if (ifs && ifs->fail())
		{
			delete ifs;
			break;
		}

		ifs->seekg(0,ifs->end);
		std::streampos end_pos = ifs->tellg();
		ifs->seekg(0,ifs->beg);
		this->total_data_size += (end_pos - ifs->tellg());

		this->data_files.push_back(ifs);
	}
	return loaded_files;
}

void VFFArchive::update(void)
{
	this->header.reset();
	this->header_file.seekg(this->hdr_ofs,this->header_file.beg);
	this->header.read_from_file(this->header_file);
}

bool VFFArchive::retrieve_file_information(VFF_STORED_FILE_INFO *info,unsigned int index)
{
	if (info)
	{
		int name_length = this->header.get_value_as_string(index,0).size() + 2;
		info->name = new (std::nothrow) char[name_length]();
		if (info->name)
		{
			strncpy(info->name,this->header.get_value_as_string(index,0).c_str(),name_length);
		}
		info->offset = std::stoll(this->header.get_value_as_string(index,1));
		info->size = std::stoll(this->header.get_value_as_string(index + 1,1)) - stoll(this->header.get_value_as_string(index,1));
		info->flag = std::stoi(this->header.get_value_as_string(index,2));
		return true;
	}
	return false;
}

void VFFArchive::release_file_information(VFF_STORED_FILE_INFO *info)
{
	if (info)
	{
		delete [] info->name;
	}
}

bool VFFArchive::locate_data_file_by_offset(__int64 offset,unsigned int *data_file_idx,__int64* adjusted_offset)
{
	if (offset >= this->total_data_size)
	{
		return false;
	}
	if (offset < this->first_data_size)
	{
		*adjusted_offset = offset;
		*data_file_idx = 0;
		return true;
	}
	auto calc_size = [](std::ifstream* f) -> __int64
	{
		std::streampos current_pos = f->tellg();
		f->seekg(0,f->end);
		std::streampos end_pos = f->tellg();
		f->seekg(0,f->beg);
		__int64 sz = end_pos - f->tellg();
		f->seekg(current_pos);
		return sz;
	};
	offset -= calc_size(this->data_files[0]);
	for(unsigned int i = 1;i < this->data_files.size();++i)
	{
		if (offset < calc_size(this->data_files[i]))
		{
			*adjusted_offset = offset;
			*data_file_idx = i;
			return true;
		}
		offset -= calc_size(this->data_files[i]);
	}
	return false;
}

unsigned int VFFArchive::read_file_data_into_buffer(unsigned char *buffer,unsigned int read_size,unsigned int file_index,__int64 read_offset)
{
	unsigned int copied_bytes = 0;
	unsigned int idx = 0;
	__int64 adjusted_offset = 0;
	VFF_STORED_FILE_INFO info = {0};
	this->retrieve_file_information(&info,file_index);
	if (this->locate_data_file_by_offset(info.offset + this->hdr_ofs + read_offset,&idx,&adjusted_offset))
	{
		auto calc_size = [](std::ifstream* f) -> __int64
		{
			std::streampos current_pos = f->tellg();
			f->seekg(0,f->end);
			std::streampos end_pos = f->tellg();
			f->seekg(0,f->beg);
			__int64 sz = end_pos - f->tellg();
			f->seekg(current_pos);
			return sz;
		};
		for(unsigned int i = idx;i < this->data_files.size();++i)
		{
			__int64 sz = calc_size(this->data_files[i]);
			this->data_files[i]->seekg(adjusted_offset,std::ifstream::beg);
			if ((adjusted_offset + read_size) <= sz)
			{
				this->data_files[i]->read(reinterpret_cast<char*>(buffer),read_size);
				copied_bytes += read_size;
				break;
			}
			this->data_files[i]->read(reinterpret_cast<char*>(buffer),sz - adjusted_offset);
			copied_bytes += (sz - adjusted_offset);
			buffer += (sz - adjusted_offset);
			read_size -= (sz - adjusted_offset);
			adjusted_offset = 0;
		}
	}
	this->release_file_information(&info); // return acquired information.
	return copied_bytes;
}
