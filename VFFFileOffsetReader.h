#ifndef VFF_FILE_OFFSET_READER
#define VFF_FILE_OFFSET_READER

#include <cstdint>
#include <fstream>
#include "IVFFArchiveDataReader.h"

class VFFFileOffsetReader : public IVFFArchiveDataReader
{
public:
	VFFFileOffsetReader();
	~VFFFileOffsetReader();
	unsigned int read_multiple_entries(std::ifstream& file,unsigned int entry_count);
	std::string get_value_as_string(unsigned int index = 0);
private:
	void release(void);

	unsigned int num_entries = 0;
	int64_t *file_offset_list = 0;
};

#endif

