#ifndef VFF_ARCHIVE_FILENAME_READER_H
#define VFF_ARCHIVE_FILENAME_READER_H

#include <fstream>
#include "IVFFArchiveDataReader.h"

class VFFArchiveFileNameReader : public IVFFArchiveDataReader
{
public:
	VFFArchiveFileNameReader();
	~VFFArchiveFileNameReader();
	unsigned int read_multiple_entries(std::ifstream& file,unsigned int entry_count);
	std::string get_value_as_string(unsigned int index = 0);
private:
	void release(void);

	unsigned int num_entries = 0;
	char** file_name_list = 0;
};

#endif

