#include <cstdint>
#include "VFFArchiveFileNameReader.h"

VFFArchiveFileNameReader::VFFArchiveFileNameReader()
{
}

VFFArchiveFileNameReader::~VFFArchiveFileNameReader()
{
	this->release();
}

void VFFArchiveFileNameReader::release(void)
{
	for(unsigned int i = 0;i < this->num_entries;++i)
	{
		delete [] (this->file_name_list[i]);
	}
	delete [] this->file_name_list;
}

unsigned int VFFArchiveFileNameReader::read_multiple_entries(std::ifstream& file,unsigned int entry_count)
{
	uint32_t crypt_key = 0;

	auto next_key = [=](uint32_t *key) -> uint32_t {
		*key += ((*key) << 2);
		return *key += 0x75d6ee39;
	};

	auto decrypt_buffer = [=](unsigned char *buf,int len,uint32_t *key) -> void {
		for(int i = 0;i < len;++i)
		{
			*(buf + i) ^= next_key(key);
		}
	};

	this->release();
	this->file_name_list = new (std::nothrow) char*[entry_count];

	if (!this->file_name_list)
	{
		return 0;
	}

	for(unsigned int i = 0;i < entry_count;++i)
	{
		unsigned int name_length = 0;
		file.read((char*)&name_length,4);
		(this->file_name_list)[i] = new char [name_length + 2]();
		if ((this->file_name_list)[i])
			file.read((this->file_name_list)[i],name_length);
		decrypt_buffer((unsigned char*)(this->file_name_list[i]),name_length,&crypt_key);
	}

	return this->num_entries = entry_count;
}

std::string VFFArchiveFileNameReader::get_value_as_string(unsigned int index)
{
	if (this->num_entries > index)
	{
		return std::string(*(this->file_name_list + index));
	}
	return std::string();
}
