#include "IVFFArchiveDataReader.h"

class HeaderDataReaderFactory
{
public:
	HeaderDataReaderFactory() {};
	~HeaderDataReaderFactory() {};
	IVFFArchiveDataReader* create(int n);
	void destroy(IVFFArchiveDataReader* p);
};

extern HeaderDataReaderFactory hdr_reader_factory;
