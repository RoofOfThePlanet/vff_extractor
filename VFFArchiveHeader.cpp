#include "HeaderDataReaderFactory.h"
#include "VFFArchiveHeader.h"

VFFArchiveHeader::VFFArchiveHeader()
{
	this->HeaderData.push_back(hdr_reader_factory.create(0));
	this->HeaderData.push_back(hdr_reader_factory.create(1));
	this->HeaderData.push_back(hdr_reader_factory.create(2));
}

VFFArchiveHeader::~VFFArchiveHeader()
{
	this->delete_all();
}

bool VFFArchiveHeader::is_valid(void)
{
	return this->signature.is_valid() && (this->data_size > ((this->num_entries * (4 + 8 + 1 + 8 + 1)) + 8 + 6));
	/* data size mst be (at least) greater than num_entries * (
		sum of
	    4 : length of name length field,
	    8 : length of offset field,
	    1 : length of flag fiedl,
	    8 : length of unknown field,
	    1 : length of unknown field
	    ) + 8 (additional offset field) + 6 (length of header signature) */
}

int VFFArchiveHeader::read_from_file(std::ifstream& file)
{
	std::streampos current_position = file.tellg();
	file.seekg(0,file.end);
	this->data_size = file.tellg() - current_position;
	file.seekg(current_position);

	this->signature.read(file);

	file.read((char*)&this->num_entries,4);

	if (this->is_valid())
	{
		for(auto& d : this->HeaderData)
		{
			d->read_multiple_entries(file,this->num_entries);
		}
	}
	return 0;
}

void VFFArchiveHeader::reset(void)
{
	this->delete_all();
	this->HeaderData.push_back(hdr_reader_factory.create(0));
	this->HeaderData.push_back(hdr_reader_factory.create(1));
	this->HeaderData.push_back(hdr_reader_factory.create(2));
}

void VFFArchiveHeader::delete_all(void)
{
	for(auto& item : this->HeaderData)
	{
		hdr_reader_factory.destroy(item);
	}
	this->HeaderData.clear();
	this->num_entries = 0;
	this->data_size = 0;
}

std::string VFFArchiveHeader::get_value_as_string(unsigned int file_index,unsigned int data_key)
{
	if (this->HeaderData.size() > data_key)
	{
		return this->HeaderData[data_key]->get_value_as_string(file_index);
	}
	return std::string();
}
