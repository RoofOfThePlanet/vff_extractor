OBJS = vffextractor.obj HeaderDataReaderFactory.obj VFFArchive.obj VFFArchiveFileNameReader.obj VFFArchiveHeader.obj VFFArchiveHeaderSignature.obj VFFFileFlagReader.obj VFFFileOffsetReader.obj ZfDecompressor.obj
LIBS = ../zlib/zlib.lib
ZLIB_DIR = ../zlib

CXX = cl
LD = link
CXXFlags = /MD /EHa /W3 /O2 /Zi /c

all: $(OBJS) $(LIBS)
	$(LD) $(OBJS) $(LIBS)

.cpp.obj:
	$(CXX) /I $(ZLIB_DIR) $(CXXFlags) $<

vffextractor.obj: vffextractor.cpp
HeaderDataReaderFactory.obj: HeaderDataReaderFactory.cpp
VFFArchive.obj: VFFArchive.cpp
VFFArchiveFileNameReader.obj: VFFArchiveFileNameReader.cpp
VFFArchiveHeader.obj: VFFArchiveHeader.cpp
VFFArchiveHeaderSignature.obj: VFFArchiveHeaderSignature.cpp
VFFFileFlagReader.obj: VFFFileFlagReader.cpp
VFFFileOffsetReader.obj: VFFFileOffsetReader.cpp
ZfDecompressor.obj: ZfDecompressor.cpp
