#ifndef VFF_FILE_FLAG_READER_H
#define VFF_FILE_FLAG_READER_H

#include <fstream>
#include "IVFFArchiveDataReader.h"

class VFFFileFlagReader : public IVFFArchiveDataReader
{
public:
	VFFFileFlagReader();
	~VFFFileFlagReader();
	unsigned int read_multiple_entries(std::ifstream& file,unsigned int entry_count);
	std::string get_value_as_string(unsigned int index = 0);
private:
	void release(void);

	unsigned int num_entries = 0;
	int* file_flag_list = 0;
};

#endif


