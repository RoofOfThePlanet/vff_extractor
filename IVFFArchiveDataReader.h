#ifndef IVFF_ARCHIVE_DATA_READER_H
#define IVFF_ARCHIVE_DATA_READER_H

#include <fstream>
#include <string>

class IVFFArchiveDataReader
{
public:
	IVFFArchiveDataReader(){};
	virtual ~IVFFArchiveDataReader(){};
	virtual unsigned int read_multiple_entries(std::ifstream& file,unsigned int entry_count) = 0;
	virtual std::string get_value_as_string(unsigned int index) = 0;
};


#endif

