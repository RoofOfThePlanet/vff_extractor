#include "HeaderDataReaderFactory.h"
#include "VFFArchiveFileNameReader.h"
#include "VFFFileOffsetReader.h"
#include "VFFFileFlagReader.h"

IVFFArchiveDataReader* HeaderDataReaderFactory::create(int n)
{
	switch(n)
	{
	case 0:
		return new VFFArchiveFileNameReader;
	case 1:
		return new VFFFileOffsetReader;
	case 2:
		return new VFFFileFlagReader;
	default:
		break;
	}
	return nullptr;
}

void HeaderDataReaderFactory::destroy(IVFFArchiveDataReader *p)
{
	delete p;
}

HeaderDataReaderFactory hdr_reader_factory;
