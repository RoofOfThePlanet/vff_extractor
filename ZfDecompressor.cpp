#include "ZfDecompressor.h"
#include <new>
#include <cassert>

#define ZLIB_WINAPI

ZfDecompressor::ZfDecompressor(const char *out_file_name) : chunk_size(1024 * 1024 * 4) , is_initialized(false) , prev_status(0)
{
	this->out_file.open(out_file_name,std::ofstream::binary);
	this->zstrm.next_in = nullptr;
	this->zstrm.avail_in = 0;
	this->zstrm.zalloc = Z_NULL;
	this->zstrm.zfree = Z_NULL;
	this->zstrm.opaque = nullptr;
	if (Z_OK == inflateInit(&this->zstrm))
	{
		this->is_initialized = true;
	}
}

ZfDecompressor::~ZfDecompressor()
{
	inflateEnd(&this->zstrm);
	this->out_file.close();
}

unsigned int ZfDecompressor::decompress(unsigned char *data,unsigned int n)
{
	unsigned int written_bytes = 0;
	if (this->is_initialized)
	{
		if (this->prev_status == Z_OK)
		{
			this->zstrm.next_in = data;
			while(n)
			{
				unsigned int read_size = ((n < this->chunk_size) ? n : this->chunk_size);
				unsigned int out_buffer_size = 2*read_size;
				this->zstrm.avail_in = read_size;
				do
				{
					try
					{
						unsigned char *out_buffer = new unsigned char[out_buffer_size];
						this->zstrm.next_out = out_buffer;
						this->zstrm.avail_out = out_buffer_size;
						int zlib_status = inflate(&this->zstrm,Z_NO_FLUSH);
						this->prev_status = zlib_status;
						if (zlib_status < 0)
						{
							delete [] out_buffer;
							break;
						}
						this->out_file.write(reinterpret_cast<char*>(out_buffer),out_buffer_size - this->zstrm.avail_out);
						delete [] out_buffer;
						written_bytes += (out_buffer_size - this->zstrm.avail_out);
					}
					catch(std::bad_alloc& e)
					{
					}
					catch(...)
					{
						assert(1);
					}
				}
				while(this->zstrm.avail_out == 0);
				n -= read_size;
			}
		}
	}
	return written_bytes;
}
